#!/bin/bash
# cmake のインストール
# ホストと GID / UID を合わせるためのグループ／ユーザの作成
# 参考 : https://github.com/devcontainers/templates/blob/main/src/cpp/.devcontainer/reinstall-cmake.sh
set -e

CMAKE_VERSION=3.27.2
CMAKE_TGZ_NAME="cmake-${CMAKE_VERSION}-linux-x86_64.tar.gz"

GROUPNAME=devgroup
USERNAME=devuser

cleanup() {
    EXIT_CODE=$?
    set +e
    if [[ -n "${TMP_DIR}" ]]; then
        rm -rf "${TMP_DIR}"
    fi
    exit ${EXIT_CODE}
}
trap cleanup EXIT

TMP_DIR=$(mktemp -d -t setup-XXXXXXXXXX)

cd "${TMP_DIR}"

curl -sSL \
  -O \
  "https://github.com/Kitware/CMake/releases/download/v${CMAKE_VERSION}/${CMAKE_TGZ_NAME}"

mkdir -p /opt/cmake
tar zxvf "${TMP_DIR}/${CMAKE_TGZ_NAME}" -C /opt/cmake --strip-components 1

ln -s /opt/cmake/bin/cmake /usr/local/bin/cmake
ln -s /opt/cmake/bin/ctest /usr/local/bin/ctest

groupadd -g 1000 "${GROUPNAME}"
useradd -m -u 1000 -g "${GROUPNAME}" -s /bin/bash "${USERNAME}"
