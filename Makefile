objects = src/app.o \
  src/lib/ui/cli/gen_command.o \
  src/lib/controller/generator.o \
  src/lib/controller/clmn_factory.o \
  src/lib/model/record/base_record.o \
  src/lib/model/column/base_type.o

data-gen : $(objects)
	gcc -o data-gen $(objects)

src/app.o : src/app.c src/app.h \
  src/lib/ui/cli/gen_command.h
	gcc -c src/app.c -o src/app.o

src/lib/ui/cli/gen_command.o : src/lib/ui/cli/gen_command.c src/lib/ui/cli/gen_command.h \
  src/lib/controller/generator.h
	gcc -c src/lib/ui/cli/gen_command.c -o src/lib/ui/cli/gen_command.o

src/lib/controller/generator.o : src/lib/controller/generator.c src/lib/controller/generator.h \
  src/lib/model/record/base_record.h \
  src/lib/model/column/base_type.h
	gcc -c src/lib/controller/generator.c -o src/lib/controller/generator.o

src/lib/controller/clmn_factory.o : src/lib/controller/clmn_factory.c src/lib/controller/clmn_factory.h \
  src/lib/model/record/base_record.h \
  src/lib/model/column/base_type.h
	gcc -c src/lib/controller/clmn_factory.c -o src/lib/controller/clmn_factory.o

src/lib/model/record/base_record.o : src/lib/model/record/base_record.c src/lib/model/record/base_record.h \
  src/lib/model/column/base_type.h
	gcc -c src/lib/model/record/base_record.c -o src/lib/model/record/base_record.o

src/lib/model/column/base_type.o : src/lib/model/column/base_type.c src/lib/model/column/base_type.h
	gcc -c src/lib/model/column/base_type.c -o src/lib/model/column/base_type.o


clean :
	rm data-gen $(objects)
