#include "app.h"

#include <stdio.h>

#include "./lib/ui/cli/gen_command.h"

/*
  メモ）
  - C 言語の規約上は argv は書き換え可能な想定であり、 const は付かない
  - CERT によると第三引数の envp を使うのはとあるリスクがあるそうなので使わない
*/
int main(int argc, char *argv[]) {
  // 内部で呼び出す関数の復帰値はエラーの種類を意味する値となる想定なので、エラーを意味する変数名で表す
  int err = 0;
  GenCommand command;

  printf("start\n");

  GenCommandConstructor(&command);
  err = GenCommandSetOpt(&command, argc, argv);
  if (err != 0) {
    fprintf(stderr, "error : %s:%d:%s\n", __FILE__, __LINE__, __func__);
    return 8;
  }
  GenCommandDispOpt(&command);

  err = GenCommandRun(&command);
  if (err != 0) {
    fprintf(stderr, "error : %s:%d:%s\n", __FILE__, __LINE__, __func__);
    return 8;
  }

  GenCommandDestructor(&command);

  printf("finish\n");

  return 0;
}
