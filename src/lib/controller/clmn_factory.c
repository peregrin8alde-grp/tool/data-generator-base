#include "clmn_factory.h"

#include <stdio.h>
#include <stdlib.h>

#include "../model/column/base_type.h"
#include "clmn_interface.h"

ColumnInterface *ColumnFactoryCreate(ClmnType type, size_t data_len) {
  ColumnInterface *ret;

  switch (type) {
    case kBaseType:
      BaseType *new_column_p = (BaseType *)malloc(sizeof(BaseType));
      BaseTypeConstructor(new_column_p, data_len);

      ret = (ColumnInterface *)new_column_p;
      ret->SetClmnTypeKey(ret, kBaseType);

      break;
    default:
      break;
  }

  return ret;
}

int ColumnFactoryCopy(ColumnInterface *dest_p, ColumnInterface *src_p) {
  int type = dest_p->GetClmnTypeKey(dest_p);
  switch (type) {
    case kBaseType:
      BaseTypeCopy((BaseType *)dest_p, (BaseType *)src_p);

      break;
    default:
      break;
  }

  return 0;
}

void ColumnFactoryDelete(ColumnInterface *interface) {
  int type = interface->GetClmnTypeKey(interface);
  switch (type) {
    case kBaseType:
      BaseTypeDestructor((BaseType *)interface);

      break;
    default:
      break;
  }

  free(interface);
}
