#ifndef CLMN_FACTORY_H
#define CLMN_FACTORY_H
#include <stdio.h>

#include "clmn_interface.h"

typedef enum clmnType {
    kBaseType = 0,
} ClmnType;

typedef struct columnFactory {
} ColumnFactory;

// ColumnInterface を持ったオブジェクトの生成方法は factory だけが知ってれば良い
ColumnInterface *ColumnFactoryCreate(ClmnType, size_t /* data_len */);

// 複製も同様
int ColumnFactoryCopy(ColumnInterface * /* dest_p */, ColumnInterface * /* src_p */);

// delete のタイミングは create を実行した側（ factory
// の利用者）が意識する話だが、 delete 方法は factory だけが知ってれば良い想定
void ColumnFactoryDelete(ColumnInterface * /* interface */);
#endif /* CLMN_FACTORY_H */
