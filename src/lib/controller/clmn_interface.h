/*
  ～することができるという意味での振る舞いの定義をしたい場合にはインターフェース（～able という名前を付けると分かりやすい？）
  ～の一種という意味での分類をしたい場合には抽象化＋継承（カプセル化まではしない）
  オブジェクトの利用者側が必要とする機能を定義するのがインターフェースなので、項目の利用者が主語となるデータのやり取り機能は
  インターフェースで良い
  ここで言うデータは項目が持つメンバとは意味が異なる。全メンバに getter / setter を用意したとして、それを抽象化して最低限のもの
  までまとめたらインターフェースとなる（ getter / setter が不要になる）？
  項目を表現するうえで必要なメソッドは抽象化となり、項目側が主語となるはず
*/
#ifndef CLMN_INTERFACE_H
#define CLMN_INTERFACE_H
#include <stdio.h>

typedef struct columnInterface {
  // 項目種別を意味するキー値を扱う。値を決めるのは factory などの管理側
  int (*GetClmnTypeKey)(struct columnInterface *);
  void (*SetClmnTypeKey)(struct columnInterface *, int /* key */);

  unsigned char *(*GetData)(struct columnInterface *);
  int (*SetData)(struct columnInterface *, unsigned char * /* data */, size_t /* data_len */);

  unsigned char *(*GetText)(struct columnInterface *);

  size_t (*GetDataLen)(struct columnInterface *);
  size_t (*GetTextLen)(struct columnInterface *);

} ColumnInterface;

#endif /* CLMN_INTERFACE_H */
