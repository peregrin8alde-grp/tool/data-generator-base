
#include "generator.h"

#include <stdio.h>
#include <string.h>

#include "../model/column/base_type.h"
#include "../model/record/base_record.h"
#include "clmn_factory.h"

void GeneratorConstructor(Generator *this_p) {}

void GeneratorDestructor(Generator *this_p) {}

int GeneratorGenerate(Generator *this_p, size_t record_num) {
  int i;

  BaseRecord record;
  ColumnInterface *col01_p = ColumnFactoryCreate(kBaseType, 10);
  ColumnInterface *col02_p = ColumnFactoryCreate(kBaseType, 20);
  unsigned char buf_coldata[kMaxColDataLen + 1] = {'\0'};

  for (i = 1; i <= record_num; i++) {
    BaseRecordConstructor(&record);

    sprintf(buf_coldata, "%d", i);
    col01_p->SetData(col01_p, buf_coldata, strlen(buf_coldata));
    BaseTypeDisp((BaseType *)col01_p);
    BaseRecordAddColmn(&record, col01_p);

    BaseRecordDisp(&record);

    col02_p->SetData(col02_p, "123456789012345", 15);
    BaseRecordAddColmn(&record, col02_p);

    BaseRecordDisp(&record);

    BaseRecordDestructor(&record);
  }

  ColumnFactoryDelete(col01_p);
  ColumnFactoryDelete(col02_p);

  return 0;
}
