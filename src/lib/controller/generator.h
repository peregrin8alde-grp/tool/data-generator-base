#ifndef GENERATOR_H
#define GENERATOR_H

#include <stdio.h>

typedef struct generator {
} Generator;

void GeneratorConstructor(Generator* /* this_p */);
void GeneratorDestructor(Generator* /* this_p */);

int GeneratorGenerate(Generator* /* this_p */, size_t /* record_num */);

#endif /* GENERATOR_H */
