#include "base_type.h"

#include <stdlib.h>
#include <string.h>

static int BaseTypeGetClmnTypeKey(struct columnInterface *interface) {
  BaseType *this_p = (BaseType *)interface;

  return this_p->clmn_type_key;
}

static void BaseTypeSetClmnTypeKey(struct columnInterface *interface, int key) {
  BaseType *this_p = (BaseType *)interface;
  this_p->clmn_type_key = key;
}

static unsigned char *BaseTypeGetData(struct columnInterface *interface) {
  BaseType *this_p = (BaseType *)interface;

  return this_p->data;
}

static int BaseTypeSetData(struct columnInterface *interface, unsigned char *data, size_t data_len) {
  BaseType *this_p = (BaseType *)interface;

  if (data_len > this_p->data_len) {
    fprintf(stderr, "error : %s:%d:%s\n", __FILE__, __LINE__, __func__);
    return 1;
  }

  memset(this_p->data, this_p->data_len, 0x00);
  memcpy(this_p->data, data, data_len);

  return 0;
}

static size_t BaseTypeGetDataLen(struct columnInterface *interface) {
  BaseType *this_p = (BaseType *)interface;

  return this_p->data_len;
}

void BaseTypeConstructor(BaseType *this_p, size_t data_len) {
  this_p->interface.GetClmnTypeKey = BaseTypeGetClmnTypeKey;
  this_p->interface.SetClmnTypeKey = BaseTypeSetClmnTypeKey;
  this_p->interface.GetData = BaseTypeGetData;
  this_p->interface.SetData = BaseTypeSetData;
  this_p->interface.GetText = NULL;
  this_p->interface.GetDataLen = BaseTypeGetDataLen;
  this_p->interface.GetTextLen = NULL;

  memset(this_p->name, '\0', kMaxColNameLen);
  this_p->data = (unsigned char *)calloc(data_len, sizeof(unsigned char));
  this_p->data_len = data_len;
}

void BaseTypeDestructor(BaseType *this_p) {
  if (this_p->data != NULL) {
    free(this_p->data);
  }
}

void BaseTypeDisp(BaseType *this_p) {
  unsigned char buf[kMaxColDataLen + 1] = {'\0'};

  if (this_p->data == NULL) {
    printf("NULL\n");
  } else {
    memcpy(buf, this_p->data, kMaxColDataLen);

    printf("%s\n", buf);
  }
}

int BaseTypeCopy(BaseType *this_p, BaseType *src_p) {
  // コンストラクタの実行者がデストラクタも実行できるように、複製時はポインタを流用せず中身のコピーを行う
  if (src_p->data_len > this_p->data_len) {
    fprintf(stderr, "error : %s:%d:%s\n", __FILE__, __LINE__, __func__);
    return 1;
  }

  memcpy(this_p->name, src_p->name, kMaxColNameLen);
  memset(this_p->data, this_p->data_len, 0x00);
  memcpy(this_p->data, src_p->data, src_p->data_len);
  
  return 0;
}
