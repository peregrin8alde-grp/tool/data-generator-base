#ifndef BASE_TYPE_H
#define BASE_TYPE_H

#include <stdio.h>

#include "../../controller/clmn_interface.h"

enum {
  kMaxColNameLen = 256,
  kMaxColDataLen = 1024,
};

typedef struct baseType {
  ColumnInterface interface;
  int clmn_type_key;
  unsigned char name[kMaxColNameLen + 1];
  unsigned char *data;
  size_t data_len;
} BaseType;

void BaseTypeConstructor(BaseType * /* this_p */, size_t /* data_len */);
void BaseTypeDestructor(BaseType * /* this_p */);

void BaseTypeDisp(BaseType * /* this_p */);
int BaseTypeCopy(BaseType * /* this_p */, BaseType * /* src_p */);

#endif
