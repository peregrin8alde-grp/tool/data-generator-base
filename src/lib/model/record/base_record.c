#include "base_record.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../../controller/clmn_factory.h"

void BaseRecordConstructor(BaseRecord *this_p) {
  this_p->firstColNode = NULL;
  this_p->lastColNode = NULL;
  this_p->colNum = 0;
}

void BaseRecordDestructor(BaseRecord *this_p) {
  ColNode *curNode = this_p->firstColNode;
  ColNode *nextNode;
  while (curNode != NULL) {
    nextNode = curNode->next;
    ColumnFactoryDelete(curNode->data);

    free(curNode);
    curNode = nextNode;
  }
}

int BaseRecordAddColmn(BaseRecord *this_p, ColumnInterface *colmn_interface) {
  ColNode *addNode = (ColNode *)malloc(sizeof(ColNode));
  if (addNode == NULL) {
    fprintf(stderr, "error : %s:%d:%s\n", __FILE__, __LINE__, __func__);
  }
  addNode->data = ColumnFactoryCreate(
      colmn_interface->GetClmnTypeKey(colmn_interface),
      colmn_interface->GetDataLen(colmn_interface));

  ColumnFactoryCopy(addNode->data, colmn_interface);

  addNode->next = NULL;

  if (this_p->firstColNode == NULL) {
    this_p->firstColNode = addNode;
    this_p->lastColNode = addNode;
  } else {
    this_p->lastColNode->next = addNode;
  }

  this_p->colNum++;
}

void BaseRecordDisp(BaseRecord *this_p) {
  // CSV 出力などがしたいわけではなく、レコードとしての情報表示がしたい
  ColNode *curNode = this_p->firstColNode;
  ColumnInterface *col_if_p;
  unsigned char *data;
  unsigned char buf[kMaxColDataLen + 1] = {'\0'};

  while (curNode != NULL) {
    col_if_p = (ColumnInterface *)curNode->data;
    data = col_if_p->GetData(col_if_p);
    if (data == NULL) {
      printf("NULL\n");
    } else {
      memcpy(buf, data, col_if_p->GetDataLen(col_if_p));

      printf("%s,", buf);
    }

    curNode = curNode->next;
  }
  printf("\n");
}
