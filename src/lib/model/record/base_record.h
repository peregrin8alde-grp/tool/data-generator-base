#ifndef BASE_RECORD_H
#define BASE_RECORD_H

#include "../../controller/clmn_interface.h"
#include "../column/base_type.h"

typedef struct colNode {
  ColumnInterface *data;
  struct colNode *next;
} ColNode;

typedef struct baseRecord {
  ColNode *firstColNode;
  ColNode *lastColNode;
  size_t colNum;
} BaseRecord;

void BaseRecordConstructor(BaseRecord * /* this_p */);
void BaseRecordDestructor(BaseRecord * /* this_p */);

int BaseRecordAddColmn(BaseRecord * /* this_p */, ColumnInterface * /* colmn_interface */);
void BaseRecordDisp(BaseRecord * /* this_p */);

#endif
