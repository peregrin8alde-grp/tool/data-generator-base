#define MAX_COLNAME_NUM 64
#define MAX_COLNAME_LEN 256

typedef struct csvFile {
  char header[MAX_COLNAME_NUM][MAX_COLNAME_LEN + 1];

  // レコードをメモリ上に保持することはないはず
  BaseRecord *records;
} CsvFile;
