/*
  IO ストリーム関連
  FILE 構造体の再開発に近いか？
  FILE だとメモリ上への入出力（ sscanf / sprintf 相当）ができない？
*/
#ifndef BASE_FILE_H
#define BASE_FILE_H

#include "../record/base_record.h"

int read();
int write();
int seek();
int can_read();
int can_write();

#endif
