/*
  コマンドライン関連
  データ生成コマンド
*/
#include "gen_command.h"

#include <string.h>
#include <unistd.h>

#include "../../controller/generator.h"

void GenCommandConstructor(GenCommand *this_p) {}

void GenCommandDestructor(GenCommand *this_p) {}

/*
  コマンドラインオプションの設定
  参考 :
  https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
*/
int GenCommandSetOpt(GenCommand *this_p, int argc, char **argv) {
  int opt;

  while ((opt = getopt(argc, argv, "o:")) != -1) {
    switch (opt) {
      case 'o':
        if (strlen(optarg) > kMaxFileName) {
          return 1;
        }
        strcpy(this_p->opt_outfile, optarg);
        break;
      case '?':
      default:
        fprintf(stderr, "Usage: %s -o outfile\n", argv[0]);

        return 1;
    }
  }

  if (strlen(this_p->opt_outfile) == 0) {
    fprintf(stderr, "Usage: %s -o outfile\n", argv[0]);

    return 1;
  }

  return 0;
}

void GenCommandDispOpt(GenCommand *this_p) {
  printf("o,%s\n", this_p->opt_outfile);
}

int GenCommandRun(GenCommand *this_p) {
  int err = 0;

  Generator generator;

  GeneratorConstructor(&generator);

  err = GeneratorGenerate(&generator, 10);
  if (err != 0) {
    fprintf(stderr, "error : %s:%d:%s\n", __FILE__, __LINE__, __func__);
    GeneratorConstructor(&generator);

    return 1;
  }

  GeneratorConstructor(&generator);

  return 0;
}
