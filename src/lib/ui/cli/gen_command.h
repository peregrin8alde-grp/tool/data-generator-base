/*
  コマンドライン関連
*/
#ifndef GEN_COMMAND_H
#define GEN_COMMAND_H

#include <stdio.h>

enum {
  kMaxFileName = 256,
};

typedef struct genCommand {
  char opt_outfile[kMaxFileName + 1];

} GenCommand;

void GenCommandConstructor(GenCommand * /* this_p */);
void GenCommandDestructor(GenCommand * /* this_p */);

// argc / argv を const にしようとしてみたら、 argv として渡す値（ main の argv 想定）が const じゃないため警告が出たので保留
int GenCommandSetOpt(GenCommand * /* this_p */, int /* argc */, char ** /* argv */);
void GenCommandDispOpt(GenCommand * /* this_p */);
int GenCommandRun(GenCommand * /* this_p */);

#endif /* GEN_COMMAND_H */
